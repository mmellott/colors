#include <stdio.h>
#include <string.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

int main(int argc, char *argv[]) {
    if(argc != 2) {
        printf("usage: color color_name");
    } else {
        char *s = argv[1];
        if(strcmp(s, "red") == 0) {
            printf(ANSI_COLOR_RED); 
        } else if(strcmp(s, "green") == 0) {
            printf(ANSI_COLOR_GREEN);
        } else if(strcmp(s, "yellow") == 0) {
            printf(ANSI_COLOR_YELLOW);
        } else if(strcmp(s, "blue") == 0) {
            printf(ANSI_COLOR_BLUE);
        } else if(strcmp(s, "magenta") == 0) {
            printf(ANSI_COLOR_MAGENTA);
        } else if(strcmp(s, "cyan") == 0) {
            printf(ANSI_COLOR_CYAN);
        } else if(strcmp(s, "reset") == 0) {
            printf(ANSI_COLOR_RESET);
        }
    }
    return 0;
}
