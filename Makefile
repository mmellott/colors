INSTALL_PREFIX=/home/mmellott
CFLAGS=-Wall -Werror -ansi

color: color.c
	gcc ${CFLAGS} color.c -o color

.PHONY: install
install:
	mkdir -p ${INSTALL_PREFIX}/bin/
	cp color ${INSTALL_PREFIX}/bin/

.PHONY: uninstall
	rm ${INSTALL_PREFIX}/bin/color

